
# Final Project

## Kelompok 4

## Anggota Kelompok

- Muhammad Ujang
- Muhammad Harits

## Tema Project

Admin Perpustakaan

## ERD

![erd](https://gitlab.com/haritsm9/final-project/-/blob/master/public/ERD.png)

## Link Tugas

- link deploy (http://47.254.253.77/final-project/public/login)

- link video (https://drive.google.com/file/d/1d1zQb-IY4Slu7JQoayEGeiZquB4wtKrs/view?usp=share_link)


