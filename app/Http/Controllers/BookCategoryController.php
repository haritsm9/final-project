<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BookCategory;

class BookCategoryController extends Controller
{
    public function index()
    {
        $bookcategorys = BookCategory::all();
        return view('bookcategory.index', compact('bookcategorys'));
    }

    public function create()
    {
        return view('bookcategory.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:100'
        ]);

        BookCategory::create($request->all());

        return redirect('/bookcategory')
        ->with('success', 'bookcategory Add successfully.');    
    }

    public function show($id)
    {
        $bookcategory = BookCategory::find($id);
        return view('bookcategory.show', compact('bookcategory'));
    }

    public function edit($id)
    {
        $bookcategory = BookCategory::find($id);
        return view('bookcategory.edit', compact('bookcategory'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:100'
        ]);

        $bookcategory = BookCategory::find($id);
        $bookcategory->update($request->all());

        return redirect('/bookcategory')
            ->with('success', 'bookcategory updated successfully.');
    }

    public function destroy($id)
    {
        $bookcategory = BookCategory::find($id);
        $bookcategory->delete();

        return redirect('/bookcategory')
            ->with('success', 'bookcategory deleted successfully.');
    }
}
