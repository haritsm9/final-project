<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;

class MemberController extends Controller
{
    public function index()
    {
        $members = Member::all();
        return view('member.index', compact('members'));
    }

    public function create()
    {
        return view('member.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:100',
            'address' => 'required',
            'phone' => 'required|max:15'
        ]);

        Member::create($request->all());

        return redirect('/member')
        ->with('success', 'Member Add successfully.');    
    }

    public function show($id)
    {
        $member = Member::find($id);
        return view('member.show', compact('member'));
    }

    public function edit($id)
    {
        $member = Member::find($id);
        return view('member.edit', compact('member'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:100',
            'address' => 'required',
            'phone' => 'required|max:15'
        ]);

        $member = Member::find($id);
        $member->update($request->all());

        return redirect('/member')
            ->with('success', 'Member updated successfully.');
    }

    public function destroy($id)
    {
        $member = Member::find($id);
        $member->delete();

        return redirect('/member')
            ->with('success', 'Member deleted successfully.');
    }
}
