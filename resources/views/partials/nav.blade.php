<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>
    <!-- Right navbar links -->
    @auth
    <ul class="navbar-nav ml-auto">
      <div class="dropdown">
          <a href="#" class="btn btn-info mr-4" data-toggle="dropdown">+</a>
      </div>
      <li class="nav-item dropdown user-menu">
        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
          <img src="{{asset('/template/dist/img/avatar5.png')}}" class="user-image img-circle elevation-2" alt="User Image">
          <span class="d-none d-md-inline text-capitalize">{{ Auth::user()->name }}</span>
        </a>
        
        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <!-- User image -->
          <li class="user-header bg-primary">
            <img src="{{asset('/template/dist/img/avatar5.png')}}" class="img-circle elevation-2" alt="User Image">
            <p>{{ Auth::user()->name }}
            </p>
          </li>
          
          <!-- Menu Footer-->
            
          <li class="user-footer">
            <a href="#" class="btn btn-default btn-flat">Profile</a>
            <a class=" btn btn-default btn-flat float-right" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
            </a>
          </li>
        </ul>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
          @csrf
          </form>
      </li>
    @endauth

    @guest
        <ul class="navbar-nav ml-auto">
          <div class="dropdown">
            <a href="#" class="btn btn-info mr-4" data-toggle="dropdown">+</a>
          </div>
          <li class="nav-item dropdown user-menu">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
              <img src="{{asset('/template/dist/img/user.png')}}" class="user-image img-circle elevation-2" alt="User Image">
              <span class="d-none d-md-inline text-capitalize">Guest</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <!-- User image -->
              <li class="user-header bg-primary">
                <img src="{{asset('/template/dist/img/user.png')}}" class="img-circle elevation-2" alt="User Image">
                <p>Guest
                </p>
              </li>
            </ul>
          </li>
        </ul>
    @endguest

        </ul>
      </li>
    </ul>

  </nav>