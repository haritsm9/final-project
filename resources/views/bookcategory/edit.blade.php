@extends('layouts.master')
@section('title')
<h3>Halaman Data Book Category </h3>
@endsection


@section('content')
<form action="/bookcategory/{{$bookcategory->id}}" method="post">
@csrf
@method('PUT')
<div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" name="name" value="{{$bookcategory->name}}">
    @error('name')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror
    
    <button type="submit" class="btn btn-primary mt-3">Submit</button>
</div>
</form>
@endsection