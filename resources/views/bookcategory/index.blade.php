@extends('layouts.master')
@section('title')
<h3>Halaman Data Book Category</h3>
@endsection



@section('content')
<a href="/bookcategory/create" class="btn btn-primary mb-4">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
                     @forelse ($bookcategorys as $key=> $bookcategory)
                    <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{ $bookcategory->name }}</td>
                        <td>
                            <form action="/bookcategory/{{$bookcategory->id}}" method="POST">
                            <a href="/bookcategory/{{$bookcategory->id}}" class="btn btn-info">Detail</a>
                            <a href="/bookcategory/{{$bookcategory->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>

                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        @endsection