@extends('layouts.master')
@section('title')
<h3>Halaman Data Member</h3>
@endsection

@section('sub-title')
{{$bookcategory->name}}    
@endsection

@section('content')
    <table class="table">
    <thead>
        <tr>
        <th scope="col">No.</th>
        <th scope="col">Judul Buku</th>
        <th scope="col">Jumlah Buku</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($books as $key => $item)
        <tr>
            <th scope="row">{{ $key +1 }}</th>
            <td>{{$item -> title}}</td>
            <td>{{$item -> total}}</td>
        </tr>
        @empty
            <h1>Data Kosong</h1>
        @endforelse
@endsection
