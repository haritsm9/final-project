@extends('layouts.master')
@section('title')
<h3>Halaman Book Category</h3>
@endsection


@section('content')
<form action="/bookcategory" method="post">
@csrf
<div class="form-group">
    <label>Category Name</label>
    <input type="text" class="form-control" name="name">
    @error('name')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror

    <button type="submit" class="btn btn-primary mt-2">Submit</button>
</div>
</form>
@endsection