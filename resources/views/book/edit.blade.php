@extends('layouts.master')
@section('title')
<h3>Halaman Data member</h3>
@endsection


@section('content')
<form action="/member/{{$member->id}}" method="post">
@csrf
@method('PUT')
<div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" name="name" value="{{$member->name}}">
    @error('name')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror
</div>
<div class="form-group">
    <label>Address</label>
    <input type="text" class="form-control" name="address" value="{{$member->address}}">
    @error('address')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror
</div>
<div class="form-group">
    <label>Phone</label>
    <input type="text" class="form-control" name="phone" value="{{$member->phone}}">
    @error('phone')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror
    
    <button type="submit" class="btn btn-primary mt-3">Submit</button>
</div>
</form>
@endsection