@extends('layouts.master')
@section('title')
<h3>Halaman Data Member</h3>
@endsection



@section('content')
<a href="/member/create" class="btn btn-primary mb-4">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
                     @forelse ($members as $key=> $member)
                    <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{ $member->name }}</td>
                    <td>{{ $member->address }}</td>
                    <td>{{ $member->phone }}</td>
                        
                        <td>
                            <form action="/member/{{$member->id}}" method="POST">
                            <a href="/member/{{$member->id}}" class="btn btn-info">Detail</a>
                            <a href="/member/{{$member->id}}/edit" class="btn btn-primary">Edit</a>
                            
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>

                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        @endsection