@extends('layouts.master')
@section('title')
<h3>Halaman Data member</h3>
@endsection


@section('content')
<form action="/member" method="post">
@csrf
<div class="form-group">
    <label>name</label>
    <input type="text" class="form-control" name="name">
    @error('name')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror
</div>
<div class="form-group">
    <label>Address</label>
    <input type="text" class="form-control" name="address">
    @error('address')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror
</div>
<div class="form-group">
    <label>Phone</label>
    <input type="text" class="form-control" name="phone">
    @error('phone')
    <span class="help-block text-danger">{{ $message }}</span>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>
@endsection