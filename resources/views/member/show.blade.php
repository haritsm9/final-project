@extends('layouts.master')
@section('title')
<h3>Halaman Data Member</h3>
@endsection


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detail member
                    </div>

                    <div class="panel-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>{{ $member->name }}</td>
                                </tr>
                                <tr>
                                    <td>Umur</td>
                                    <td>{{ $member->address }}</td>
                                </tr>
                                <tr>
                                    <td>Biodata</td>
                                    <td>{{ $member->phone }}</td>
                                </tr>
                            </tbody>
                        </table>

                        </form>
                        <a href="/member" class="btn btn-secondary">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
