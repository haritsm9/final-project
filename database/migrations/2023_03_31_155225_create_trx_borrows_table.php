<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_borrows', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('members_id');
            $table->foreign('members_id')
                  ->references('id')
                  ->on('members')
                  ->onDelete('cascade');
            $table->unsignedBigInteger('books_id');
            $table->foreign('books_id')
                  ->references('id')
                  ->on('books')
                  ->onDelete('cascade');
            $table->timestamp('borrowed_at')->nullable();
            $table->timestamp('due_return_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trx_borrows', function (Blueprint $table) {
            $table->dropForeign(['members_id']);
            $table->dropForeign(['books_id']);
        });
        Schema::dropIfExists('trx_borrows');
    }
};
