<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\BookCategoryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.master');
// });

Route::get('/', function () {
    return view('dashboard');
})->middleware(['auth']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/member', [MemberController::class, 'index']);
Route::post('/member', [MemberController::class, 'store']);
Route::get('/member/create', [MemberController::class, 'create']);
Route::get('/member/{member_id}', [MemberController::class, 'show']);
Route::get('/member/{member_id}/edit', [MemberController::class, 'edit']);
Route::put('/member/{member_id}', [MemberController::class, 'update']);
Route::delete('/member/{member_id}', [MemberController::class, 'destroy']);


Route::get('/bookcategory', [BookCategoryController::class, 'index']);
Route::post('/bookcategory', [BookCategoryController::class, 'store']);
Route::get('/bookcategory/create', [BookCategoryController::class, 'create']);
Route::get('/bookcategory/{bookcategory_id}', [BookCategoryController::class, 'show']);
Route::get('/bookcategory/{bookcategory_id}/edit', [BookCategoryController::class, 'edit']);
Route::put('/bookcategory/{bookcategory_id}', [BookCategoryController::class, 'update']);
Route::delete('/bookcategory/{bookcategory_id}', [BookCategoryController::class, 'destroy']);